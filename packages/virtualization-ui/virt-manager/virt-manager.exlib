# Copyright 2009 Ingmar Vanhassel
# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require setup-py [ import='setuptools' blacklist='2' test=pytest ]
require freedesktop-desktop gsettings gtk-icon-cache

export_exlib_phases src_install pkg_postrm pkg_postinst

SUMMARY="Virtual Machine Manager"
DESCRIPTION="
The 'Virtual Machine Manager' application (virt-manager for short package name)
is a desktop user interface for managing virtual machines. It presents a
summary view of running domains, their live performance & resource utilization
statistics. The detailed view graphs performance & utilization over time.
Wizards enable the creation of new domains, and configuration & adjustment of a
domain's resource allocation & virtual hardware. An embedded VNC client viewer
presents a full graphical console to the guest domain.
"
HOMEPAGE="https://${PN}.org"
DOWNLOADS="${HOMEPAGE}/download/sources/${PN}/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/docutils[python_abis:*(-)?] [[ note = [ For rst2man ] ]]
        sys-devel/gettext[>=0.19.6]
    build+run:
        dev-libs/vte:2.91[gobject-introspection]
        dev-python/libxml2[python_abis:*(-)?]
        gnome-bindings/pygobject:3[>=3.31.3][python_abis:*(-)?]
        virtualization-lib/libvirt[>=0.9.11]
        virtualization-lib/libvirt-python[>=0.7.0][python_abis:*(-)?]
        x11-libs/gtk+:3[>=3.22][gobject-introspection]
        x11-libs/pango[gobject-introspection]
    run:
        dev-libs/gtk-vnc:2.0[>=0.3.8]
        dev-libs/libosinfo:1.0[>=0.2.10][gobject-introspection]
        dev-python/requests[python_abis:*(-)?]
        dev-python/urllib3[python_abis:*(-)?]
        gnome-desktop/dconf
        gnome-desktop/gtksourceview:4.0[gobject-introspection]
        sys-apps/dbus[X]
        virtualization-lib/libvirt-glib[>=0.0.9][gobject-introspection]
        virtualization-lib/spice-protocol[>=0.10.1]
        virtualization-ui/spice-gtk:3.0[>=0.19][gobject-introspection]
    suggestion:
        net-misc/openssh [[ description = [ Required for connections through ssh ] ]]
"

# All tests containing <rng model="virtio"> fail
RESTRICT="test"

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    edo sed -e "/'install': my_install,/d" -i setup.py
}

configure_one_multibuild() {
    setup-py_configure_one_multibuild

    # config is different from configure for some reason...
    edo ${PYTHON} -B setup.py configure --prefix=/usr
}

install_one_multibuild() {
    # Well, this used to work, but it doesn't anymore because python_bytecompile
    # takes no arguments. Not every package installs to python_get_sitedir.
    # python_bytecompile /usr/share/${PN}

    # Thus, we're using our own modified stuff:
    edo ${PYTHON} -B setup.py                       \
                --no-compile-schemas                \
                --no-update-icon-cache              \
                install                             \
                --root="${IMAGE}"                   \
                --install-data=/usr                 \
                --prefix=/usr/$(exhost --target)    \
                --no-compile

    python_dir="${IMAGE}"/usr/share/${PN}
    edo find "${python_dir}" -type f -name '*.py' -exec touch {} +
    edo python$(python_get_abi)    -mcompileall -f -q -d "${python_dir}" "${python_dir}"
    edo python$(python_get_abi) -O -mcompileall -f -q -d "${python_dir}" "${python_dir}"

    keepdir /usr/share/virt-manager/virtconv/parsers
    keepdir /usr/share/virt-manager/pixmaps

    emagicdocs
}

virt-manager_src_install() {
    setup-py_src_install
}

virt-manager_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

virt-manager_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

